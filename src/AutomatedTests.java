import com.objectdb.Utilities;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.Collection;

public class AutomatedTests {
    public static void main(String[] args) {
        System.out.println("Begin tests");
        runTests(args.length >= 1 ? args[0] : "database.odb");
        System.out.println("End tests");
    }

    private static void runTests(String databaseFile) {
        PersistenceManager pm = Utilities.getPersistenceManager(databaseFile);
        String inputString = "Abraham Lincoln";
        Person person = new Person(inputString);
        assert person.getName().equals(inputString);
        if (!pm.isClosed()) {
            pm.close();
        }
    }
}
